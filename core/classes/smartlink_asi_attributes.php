<?php

/**
 * Attributes Class
 */
final class SmartLink_ASI_Attributes
{
	public static function get_attributes($product, $names_only = false)
	{
		$args = [];

		foreach ((array) @$product->Attributes as $attribute => $values) {

			if (! isset($values->Values)) continue;

			$data = [];

			foreach ($values->Values as $i => $value) {
				if ($names_only == true) {
					$data[] = str_replace(
						[ '\"', "'" ],
						[ '”', '’' ],
						$value->Name
					);
				} else {
					$data[$i] = [
						'name'	=> str_replace(
							[ '\"', "'" ],
							[ '”', '’' ],
							$value->Name
						),
						'price'	=> 0.00,
					];
				}
			}

			$args[ strtolower( $attribute ) ] = $data;

		}

		if (isset($product->HasRushService) && $product->HasRushService === true) {
			if ($names_only == true) {
				$args['rush_service'] = ['Yes', 'No'];
			} else {
				$args['rush_service'] = [
					[
						'name'	=> 'Yes',
						'price' => 0.00,
					],
					[
						'name'	=> 'No',
						'price' => 0.00,
					],
				];
			}
		}

		if ($names_only == true) $args = $args + self::get_other_attributes($product);

		return $args;
	}

	private static function get_other_attributes($product)
	{
		$args = [];

		if (isset($product->IsNew)) {
			$args['_n'] = $product->IsNew === true;
		}

		if (isset($product->HasRushService)) {
			$args['_rsv'] = $product->HasRushService === true;
		}

		if (isset($product->Origin)) {
			/*
			 * Some values in the array may have the U.S.A string
			 * So we have to join the array content and look for the U.S.A string
			 */
			$join = join(', ', $product->Origin);
			$args['_miu'] = stripos($join, 'U.S.A') !== false;
		}

		if (isset($product->HasFullColorProcess)) {
			$args['_fcp'] = $product->HasFullColorProcess === true;
		}

		if (isset($product->Imprinting->SoldUnimprinted)) {
			$args['_su'] = $product->Imprinting->SoldUnimprinted === true;
		}

		if (isset($product->IsEcoFriendly)) {
			$args['_ef'] = $product->IsEcoFriendly === true;
		}

		return $args;
	}
}
<?php

/**
 * Quantity Prices Class
 */
final class SmartLink_ASI_Quantity_Prices
{
	public static function get_prices($product)
	{
		$currency = get_option('woocommerce_currency', '');
		$args = [];

		foreach ($product->Prices as $price) {
			if (
				strtoupper($price->CurrencyCode) != strtoupper($currency) ||
				! isset($price->Quantity)
			) continue;

			if (! isset($price->Price) || ! isset($price->Quantity->From)) continue;

			$args[$price->Quantity->From] = $price->Price;
		}

		return $args;
	}
}
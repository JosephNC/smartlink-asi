<?php

use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;

/**
 * Shipping Class
 */
final class SmartLink_ASI_Shipping
{
	public static function get_shipping($product)
	{
		$dimensions = isset($product->Shipping->Dimensions) ? self::get_dimensions($product->Shipping->Dimensions) : [
			'length'	=> '',
			'width'		=> '',
			'height'	=> '',
		];
		
		$args = [
			'weight' => isset($product->Shipping->Weight) ? self::get_weight($product->Shipping->Weight) : '',
		] + $dimensions;

		return $args;
	}

	private static function get_weight($weight)
	{
		$wc_unit = get_option('woocommerce_weight_unit', 'lbs');

		$return = '';

		if (! isset($weight->Values[0]->Name)) return $return;

		$v = explode(' ', $weight->Values[0]->Name);

		$whole_number = (int) $v[0]; // Whole Number

		if (count($v) == 3) {
			$fraction = $v[1]; // Fraction
			$fraction = explode('/', $fraction);
			$unit = $v[2];
		} else {
			$fraction = [0, 1]; // Fraction
			$unit = $v[1];
		}

		$numerator = (int) $fraction[0];
		$denominator = (int) $fraction[1];

		$value = (float) ( ($whole_number * $denominator) + $numerator ) / $denominator;

		if ($wc_unit == $unit) return $value;

		try {
			$quantity = new Mass($value, $unit);
			$return = $quantity->toUnit($wc_unit);
		} catch (Exception $e) {}

		return $return;
	}

	private static function get_dimensions($dimensions)
	{
		$wc_unit = get_option('woocommerce_dimension_unit', 'in');

		$args = [
			'length'	=> '',
			'width'		=> '',
			'height'	=> '',
		];

		try {
			if (isset($dimensions->Length) && isset($dimensions->LengthUnit)) {
				$length = new Length($dimensions->Length, strtolower($dimensions->LengthUnit));
				$args['length'] = $length->toUnit($wc_unit);
			}
		} catch (Exception $e) {}

		try {
			if (isset($dimensions->Width) && isset($dimensions->WidthUnit)) {
				$width = new Length($dimensions->Width, strtolower($dimensions->WidthUnit));
				$args['width'] = $width->toUnit($wc_unit);
			}
		} catch (Exception $e) {}

		try {
			if (isset($dimensions->Height) && isset($dimensions->HeightUnit)) {
				$height = new Length($dimensions->Height, strtolower($dimensions->HeightUnit));
				$args['height'] = $height->toUnit($wc_unit);
			}
		} catch (Exception $e) {}

		return $args;
	}
}
<?php

/**
 * Categories Class
 */
final class SmartLink_ASI_Categories
{
	public static function get_category_ids($product)
	{
		$args = [];

		foreach ((array) @$product->Categories as $category) {

			if (! isset($category->Name)) continue;

			if (isset($category->Parent)) {
				// $parent = get_term_by('name', $category->Parent->Name, 'product_cat');

				$parent = self::insert_term($category->Parent->Name, 'product_cat');
				if (empty($parent)) continue;

				$args[] = $parent;
			}

			$terms = get_terms([
				'name'                   => $category->Name,
				'parent'                 => $parent ?? '',
				'get'                    => 'all',
				'number'                 => 1,
				'taxonomy'               => 'product_cat',
				'update_term_meta_cache' => false,
				'orderby'                => 'none',
				'suppress_filter'        => true,
			]);

			if ( is_wp_error( $terms ) || empty( $terms ) ) {
				$term_id = self::insert_term($category->Name, 'product_cat', $parent ?? 0);

				if (empty($term_id)) continue;
			} else {
				$term = array_shift( $terms );
				$term_id = $term->term_id;
			}

			$args[] = $term_id;
		}

		return array_filter(array_unique($args));
	}

	public static function get_theme_ids($product)
	{
		$args = [];

		foreach ((array) @$product->Themes as $theme) {

			$term_id = self::insert_term($theme, 'product_tag');

			if (empty($term_id)) continue;

			$args[] = $term_id;
		}

		return array_filter(array_unique($args));
	}

	private static function insert_term($name, $taxonomy, $parent = 0)
	{
		$term = wp_insert_term($name, $taxonomy, [
			'slug'		=> sanitize_title($name),
			'parent'	=> $parent,
		]);

		if (is_wp_error($term)) {
			$term_id = $term->error_data['term_exists'] ?? null;
		} else {
			$term_id = $term['term_id'];
		}

		return $term_id;
	}
}
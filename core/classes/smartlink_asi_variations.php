<?php

use function BenTools\CartesianProduct\cartesian_product;

/**
 * Variations Class
 */
final class SmartLink_ASI_Variations
{
	public static function get_variations($product)
	{
		if (! $product || empty($product)) return null;

		$prices		= SmartLink_ASI_Quantity_Prices::get_prices($product);
		$attributes = SmartLink_ASI_Attributes::get_attributes($product);
		$quantities	= self::get_quantities($prices);
		$collection = $quantities + $attributes;

		return self::bentool($collection);
	}

	private static function get_quantities($prices)
	{
		foreach ($prices as $quantity => $price) {
			$data[] = [
				'name'	=> $quantity,
				'price'	=> floatval($price),
			];
		}

		$args['quantity'] = $data;

		return $args;
	}

	private static function bentool($arrays)
	{
		$start	= microtime(true);
		$result = [];

		foreach (cartesian_product($arrays) as $i => $combination) {
	        $result[$i] = [
	        	'attributes'	=> [],
	        	'price'			=> 0,
	        ];

			foreach ($combination as $key => $value) {
	        	$result[$i]['attributes'][ rtrim($key, 's') ] = $value['name'];
	        	$result[$i]['price'] += $value['price'];
			}
		}

	    $time_elapsed_secs = microtime(true) - $start;
	    // var_dump($time_elapsed_secs);

		return $result;
	}
}
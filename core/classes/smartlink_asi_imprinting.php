<?php

/**
 * Imprinting Class
 */
final class SmartLink_ASI_Imprinting
{
	private static $currency = 'USD';

	public static function get_imprinting($product)
	{
		$args = [];

		self::$currency = get_option('woocommerce_currency', '');

		$colors		= self::get_attributes(@$product->Imprinting->Colors->Values);
		$methods	= self::get_attributes(@$product->Imprinting->Methods->Values);
		// $services	= self::get_attributes(@$product->Imprinting->Services->Values);
		$locations	= self::get_attributes(@$product->Imprinting->Locations->Values);
		$options	= self::get_options(@$product->Imprinting->Options);

		if (! empty($colors)) $args['imprinting_colors']		= $colors;
		if (! empty($methods)) $args['imprinting_methods']		= $methods;
		// if (! empty($services)) $args['imprinting_services']	= $services;
		if (! empty($locations)) $args['imprinting_locations']	= $locations;
		if (! empty($options)) $args['imprinting_options']		= $options;

		return $args;
	}

	private static function get_attributes($values)
	{
		$data = [];

		if (empty($values)) return $data;

		foreach ($values as $value) {
			$name = is_object($value) ? $value->Name : (string) $value;
			$name = strlen($name) >= 50 ? substr($name, 0, 45) . '...' : $name;

			if (isset($value->Charges)) {

				$prices = [];

				foreach ($value->Charges as $charge) {
					$type = $charge->TypeCode ?? 'NONE';

					foreach ($charge->Prices as $price) {
						if (! isset($price->Price)) continue;

						if (
							strtoupper($price->CurrencyCode) != strtoupper(self::$currency) ||
							! isset($price->Quantity)
						) continue;

						$prices[$type][$price->Quantity->From] = floatval($price->Price);
					}
				}

				$price = array_filter($prices);
			}

			$data[] = [
				'name'	=> trim(str_replace(
					[ '\"', "'" ],
					[ '”', '’' ],
					$name
				)),
				'price'	=> $price ?? [],
			];
		}

		return $data;
	}

	private static function get_options($options)
	{
		$data = [];

		if (empty($options)) return $data;

		$options = $options->Imprinting->Options ?? $options;

		foreach ($options as $option) {
			if (! isset($option->Name)) continue;

			$groups = $option->Values ?? @$option->Groups;

			if (! $groups || $groups == null) continue;

			$data[$option->Name] = self::get_attributes($groups);
		}

		return $data;
	}
}
<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die;

if ( ! class_exists( 'SmartLink_Admin' ) ) : 

final class SmartLink_Admin {

    private $slug = 'smartlink';

    /**
    * The Constructor
    */
    public function __construct()
    {
        global $smartlink;

        add_action( 'admin_head', array( $this, 'admin_head' ), 9999 );

        add_action( 'admin_notices', array( $this, 'display_notices' ), 9999 );

        if ( $smartlink->smartlink_inactive ) return;

        add_action( 'admin_menu', array( $this, 'admin_menu' ), 9 );

        add_action( 'add_meta_boxes_product', array( $this, 'product_metabox' ) );

        add_action( 'admin_enqueue_scripts',  array( &$this, 'admin_scripts' ), 10 );

    }

    /**
    * Product Meta Box to the Edit Screen
    */
    public function product_metabox()
    {
        add_meta_box( 'smartlink_product_metabox', 'Product Meta', array( $this, 'smartlink_product_metabox' ), $this->key, 'side', 'high' );
    }

    /**
    * HTML content to add to product metabox
    */
    public function smartlink_product_metabox()
    {
        $post_id    = get_the_ID();
        $pnu        = get_post_meta($post_id, '_PNU', true);
        ?>
        <div style="padding: 1.5%;">
            <ul style="margin: 0%;">
                <li style="margin: 2% 0;">
                <?php printf(__('Product Number: %s', 'smartlink'), $pnu); ?>
                </li>
            </ul>
        </div>
        <?php
    }

    /**
    * Display registered notices
    */
    public function display_notices()
    {
        echo SmartLink::$_notice;
    }

    /**
    * Admin Head content
    */
    public function admin_head()
    {
        ?>
        <script>
        if ( typeof jQuery != "undefined" ) {
            jQuery(document).ready( function ($) {
            });
        }
        </script>
        <?php
    }
    
    /**
    * Admin Menu
    */
	public function admin_menu()
    {
        // Include/Require files
        $this->includes();
        
        add_menu_page(
            SMARTLINK_NAME,
            SMARTLINK_NAME,
            'manage_options',
            $this->slug,
            array( $this, 'settings' ),
            SMARTLINK_URL . 'assets/images/icon.png',
            52.28473
        );
        
        add_submenu_page(
            '_smartlink_start_doesnt_exist',
            __( 'Getting Started | ', 'smartlink' ) . SMARTLINK_NAME,
            '',
            'manage_options',
            'smartlink-start',
            [$this, 'getting_started']
        );

	}
    
    /**
    * Include/Require files in the admin page
    */
    private function includes()
    {
    }
    
    /**
    * Load Admin page
    */
    public function settings()
    {
        global $smartlink;
        
        $settings = get_option( SMARTLINK_OPTIONS, array() );
        
        $options = $settings;
        
        if ( isset( $_POST['smartlink_submit'] ) ) {
            
            if ( ! check_admin_referer( 'smartlink_nonce', 'smartlink_settings' ) ) return;
            
            $error = false;

            foreach ( $_POST as $name => $value ) {
                
                if ( strpos( $name, 'smartlink_' ) === false ) continue;
                
                if ( $value == '' ) {
                    
                    $error = true;
                    
                    break;
                    
                }
                
            }
            
            if ( ! $error ) {

                $args = wp_parse_args( array(
                    'endpoint'      => sanitize_text_field( $_POST['smartlink_endpoint'] ),
                    'client_id'     => sanitize_text_field( $_POST['smartlink_client_id'] ),
                    'client_secret' => sanitize_text_field( $_POST['smartlink_client_secret'] ),
                ), $options );
                
                $settings = $args;
                
                $update = update_option( SMARTLINK_OPTIONS, $settings );
                
                if ( $update || $smartlink->is_array_equal( $args, $options ) ) {
                    
                    $smartlink->add_notice( __( '<strong> Done! </strong> Settings saved.', 'smartlink' ), 'updated', true, true );
                    
                    $options = $args;
                    
                } else {
                    
                    $smartlink->add_notice( __( '<strong> Failed! </strong> Error occurred.', 'smartlink' ), 'error', true, true );
                    
                }
                    
            } else {
                
                $smartlink->add_notice( __( '<strong> Doing wrong! </strong> All fields are required.', 'smartlink' ), 'error', true, true );
                
            }
            
        }
        
        extract( $options );
        
        require_once SMARTLINK_PATH . 'admin/templates/settings.php';        
    }
    
    /**
    * Load the getting Started template
    */
    public function getting_started() {
        
        global $smartlink_getting_started_tabs;
        
        if ( ! isset( $_GET['page'] ) || strpos( $_GET['page'], 'smartlink-' ) === false ) return;
            
        $options = get_option( SMARTLINK_OPTIONS, false );
        
        $string = @end(explode( '-', $_GET['page'])); // Surpress Only variables should be passed by reference notice
        
        if ( ! array_key_exists( $string, $smartlink_getting_started_tabs ) ) return;
        
        include_once SMARTLINK_PATH . 'admin/templates/welcome/header.php';
        
        include_once SMARTLINK_PATH . 'admin/templates/welcome/' . $string . '.php';
        
        include_once SMARTLINK_PATH . 'admin/templates/welcome/footer.php';
        
    }
    
    /**
    * Enqueue Admin Scripts
    */
    public function admin_scripts() {
        
        $screen = get_current_screen();
        
        if ( ! isset( $screen->id ) ) return;
        
        if ( strstr( $screen->id, 'smartlink' ) == false ) return;
        
        /**
        * Enqueue Styles
        */
        wp_enqueue_style( SMARTLINK_NAME, SMARTLINK_URL . 'assets/css/admin.css', array(), SMARTLINK_VERSION, 'all' );
        
        /**
        * Enqueue Scripts
        */
        wp_enqueue_script( SMARTLINK_NAME . ' URL SCRIPT', SMARTLINK_URL . 'assets/js/uri.min.js', array(), SMARTLINK_VERSION, true );
        wp_enqueue_script( SMARTLINK_NAME, SMARTLINK_URL . 'assets/js/admin.js', array(), SMARTLINK_VERSION, true );
        
    }

}

endif;

if ( is_admin() ) new SmartLink_Admin();
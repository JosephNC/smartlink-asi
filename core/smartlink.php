<?php

if (! class_exists('SmartLink_ASI')):

final class SmartLink_ASI {

	private $base_url		= '';
	private $endpoint		= '';
	private $client_id		= '';
	private $client_secret	= '';
	private $admin_id		= 0;
	private $boolean_attrs	= [];
	private $smartlink;

	/**
	 * The Constructor
	 */
	public function __construct() {
		global $smartlink;

		if ($smartlink->smartlink_inactive) return;

		$options = get_option(SMARTLINK_OPTIONS);

		$this->base_url			= @$options['endpoint'];
		$this->endpoint			= @$options['endpoint'];
		$this->client_id		= @$options['client_id'];
		$this->client_secret	= @$options['client_secret'];
		$this->admin_id			= @$options['admin_id'];
		$this->boolean_attrs	= [
    		'_n'	=> 'New',
    		'_rsv'	=> 'Rush Service',
    		'_miu'	=> 'Made in USA',
    		'_fcp'	=> 'Full Color Process',
    		'_su'	=> 'Sold Unimprinted',
    		'_ef'	=> 'Eco Friendly',
		];
		$this->smartlink		= $smartlink;

		// add_action('init', array($this, 'register_taxonomy'), 9999);
		add_action('init', array($this, 'init'), 9999);
	}

	public function init()
	{
		$this->register_taxonomy();
		$this->add_wc_attributes();
		$this->includes();

		add_action('smartlink_cron_job', array($this, 'cron_job'));

		if ( ! is_admin() || ! isset($_REQUEST['smartlink_load']) ) return;

		$this->work(
			(bool) $_REQUEST['smartlink_load_check'] ?? true,
			(int) $_REQUEST['smartlink_load_limit'] ?? 0
		);
	}

	private function register_taxonomy()
	{
		$name = 'quantity';

		$register = register_taxonomy($name, 'product_variation', [
			'label'			=> ucwords(str_replace('_', ' ', $name)),
			'public'		=> false,
			'rewrite'		=> false,
			'hierarchical'	=> true,
		]);

		$assign = register_taxonomy_for_object_type($name, 'product_variation');
	}

    public function add_wc_attributes()
    {
    	delete_transient( 'wc_attribute_taxonomies' );

    	$attributes = $this->boolean_attrs;

		$current = (array) get_transient('smartlink_boolean_attributes');

		if (count($current) == count($attributes)) return;

		foreach ($attributes as $attribute) {
			$this->process_add_attribute($attribute);
		}

		set_transient( 'smartlink_boolean_attributes', $attributes, (60 * 60) * 24);
    }

	private function process_add_attribute($attribute)
	{
		global $wpdb;

	    $table = $wpdb->prefix . 'woocommerce_attribute_taxonomies';
	    $attr_id = $wpdb->get_var("SELECT `attribute_id` FROM $table WHERE attribute_label = '$attribute'");

	    $slug = sanitize_title($attribute);

	    if ((int) $attr_id == 0) {
			$attribute = wc_create_attribute([
				'name' => $attribute,
				'slug' => $slug,
				'type' => 'select',
			]);
		}

		foreach (['Yes', 'No'] as $boolean) {
	        $insert = wp_insert_term($boolean, 'pa_' . $slug, [
	            	'slug' => sanitize_title($boolean),
	            ]
	        );
		}
	}

	public function cron_job()
	{
		$this->work();
	}

	private function includes()
	{
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_quantity_prices.php';
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_attributes.php';
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_imprinting.php';
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_variations.php';
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_categories.php';
		require_once SMARTLINK_PATH . 'core/classes/smartlink_asi_shipping.php';
	}

	private function work($check = true, $limit = 0)
	{
		$product_exist	= get_option('smartlink_product_exist', []);
		$offset_partial	= get_option('smartlink_product_loop_offset_partial', false);
		$offset			= $offset_partial !== false ? $offset_partial : get_option('smartlink_product_loop_offset', false);
		$offset			= $offset !== false ? (int) $offset : 0;
		$page			= get_option('smartlink_product_page', 1);
		$rpp			= 100;

		$result = $this->get_smartlink_search([
			'q'		=> 'quantity:1,has_price:1,has_image:1',
			'rpp'	=> $rpp, // The Maximum for ASI v2.0
			'page'	=> $page,
		]);

		if (empty($result) || empty($result->Results)) return;

		$products = $result->Results;
		$total = count($result->Results);
		$i = 0;

		for (; $offset < $total;) {

			if ($limit > 0 && $i == $limit) break;

			$product = $products[$offset];

			update_option('smartlink_product_loop_offset_partial', $offset);

			$offset++;

			if (
				$this->smartlink->get_pid_by_meta('_PID', $product->Id) == 0
			) {
				if (! in_array($product->Id, array_keys($product_exist))) {

					try {
						$insert = $this->insert_smartlink_product($product->Id, $check);
					} catch (Exception $e) {
						$this->smartlink->log($e->getMessage());
						$this->smartlink->log($e->getFile());
						$this->smartlink->log($e->getTrace());
					}

					if (is_int($insert) && $insert > 0) $product_exist += [$product->Id => $insert];

					update_option('smartlink_product_exist', $product_exist);

					if (is_array($insert) && isset($insert['message']))
						error_log('Error Occurred: ' . $insert['message']);
				}
			}

			update_option('smartlink_product_loop_offset', $offset);
			update_option('smartlink_product_loop_offset_partial', false);

			$i++;
		}

		if ($limit == 0) {
			// Reset the offset and restart the work with real check.
			update_option('smartlink_product_loop_offset', false);
			update_option('smartlink_product_loop_offset_partial', false);
			update_option('smartlink_product_page', ++$page);

			if (! isset($result->Links->Next)) {
				update_option('smartlink_product_end', true);
			} else {
				$this->work(true);
			}
		}
	}

	private function insert_smartlink_product($item_id, $check = false)
	{
		global $wpdb;

		$product = $this->get_smartlink_product($item_id);

		if (! $product || empty($product)) return [
			'type'		=> "error",
			'message'	=> "Cannot retrieve product: $item_id",
		];

		$product->Id = $product->Id ?? $item_id;

		$count = isset($product->Variants) ? count($product->Variants) : 1;

		for ($i = 0; $i < $count; $i++) {
			if ($count > 1) {
				$is_variant				= true;
				$variant_name			= $product->Name . ' - ' . substr($product->Variants[$i]->Description, 0, 20) . ' | ' . $product->Variants[$i]->Id;
				$variant_number			= $product->Variants[$i]->Number ?? $product->Number . '-' . $product->Variants[$i]->Id;
				$product->ImageUrl		= $product->Variants[$i]->ImageUrl ?? $product->ImageUrl;
				$product->Images		= $product->Variants[$i]->Images ?? [];
				$product->Attributes	= $product->Variants[$i]->Attributes ?? $product->Attributes;
				$product->Prices		= $product->Variants[$i]->Prices ?? [];
				$product->PriceIncludes	= $product->Variants[$i]->PriceIncludes ?? '';
			}

			if (
				! isset($product->Prices) || empty($product->Prices) ||
				( ! isset($product->Name) && ! isset($variant_name) ) ||
				! isset($product->Description) ||
				! isset($product->Categories) ||
				! isset($product->ImageUrl)
			) return [
				'type'		=> "error",
				'message'	=> "Price, Name, Desc, Cats or Image does not exist for product: $item_id",
			];

			$prices	= SmartLink_ASI_Quantity_Prices::get_prices($product);

			if (empty($prices)) return [
				'type'		=> "error",
				'message'	=> "Cannot retrieve price for product: $item_id",
			];

			$pname = str_replace(
				[ '\"', "'", '"' ],
				[ '”', '’', '”' ],
				$variant_name ?? $product->Name
			);
			$pnumber = $variant_number ?? $product->Number;

			$quantities = ['quantity' => array_keys($prices)];
			$variations = SmartLink_ASI_Variations::get_variations($product);
			$attributes = SmartLink_ASI_Attributes::get_attributes($product, true);
			$imprinting = SmartLink_ASI_Imprinting::get_imprinting($product);
			$attributes = $quantities + $attributes;

			foreach ($attributes as $key => $attribute) {
				unset($attributes[$key]);
				$attributes[rtrim($key, 's')] = $attribute;
			}

			$categories	= SmartLink_ASI_Categories::get_category_ids($product);
			$themes		= SmartLink_ASI_Categories::get_theme_ids($product);
			$shipping	= SmartLink_ASI_Shipping::get_shipping($product);
			$brands		= $product->TradeNames ?? (array) @$product->LineNames;
			$smartlink_info	= [];

			if (isset($product->Packaging) && ! empty($product->Packaging)) {
				$smartlink_info['packaging'] = implode("\n", array_map(function ($package) {
					$values = [];

					if (isset($package->Values)) {
						$names = [];

						foreach ($package->Values as $value)
							$names[] = is_object($value) || isset($value->Name) ? $value->Name : $value;

						$values += [implode(', ', $names)];
					}

					if (isset($package->Groups) && is_array($package->Groups)) {
						$names = [];

						foreach ($package->Groups as $group) {
							if (! isset($group->Name)) continue;

							$names[] = $group->Name;
						}

						$values += [implode(', ', $names)];
					}

					return implode(', ', $values);

				}, $product->Packaging));
			}

			if (isset($product->ProductionTime) && ! empty($product->ProductionTime)) {
				$smartlink_info['production_time'] = implode(', ', array_map(function ($production) {
					return $production->Name;
				}, $product->ProductionTime));
			}

			if (isset($product->RushTime) && ! empty($product->RushTime)) {
				$smartlink_info['rush_time'] = implode(', ', array_map(function ($rush) {
					return $rush->Description ?? $rush->Name;
				}, $product->RushTime));
			}

			if (isset($product->PriceIncludes) && ! empty($product->PriceIncludes) && is_string($product->PriceIncludes)) {
				$smartlink_info['price_includes'] = $product->PriceIncludes;
			}

			if (isset($product->AdditionalInfo) && ! empty($product->AdditionalInfo) && is_string($product->AdditionalInfo)) {
				$smartlink_info['additional_info'] = $product->AdditionalInfo;
			}

			if (isset($product->Imprinting->Sizes->Values) && ! empty($product->Imprinting->Sizes->Values)) {
				$smartlink_info['imprint_area'] = implode(', ', array_map(function ($size) {
					return @$size->Name;
				}, $product->Imprinting->Sizes->Values));
			}

		    if ($check === true || $check === false) {
		    	$product_exist = get_option('smartlink_product_exist', []);

		    	if (array_key_exists($product->Id, $product_exist)) {
		    		$post_id = $product_exist[$product->Id];
		    	} else {
					$query = "
						SELECT ID FROM $wpdb->posts
						WHERE post_author = '" . $this->admin_id . "'
						AND post_title = '" . htmlspecialchars_decode($pname) . "'
						AND post_type = 'product'
					";
					$post_id = (int) $wpdb->get_var($query);
				}

				if ($post_id > 0) {
					$sku	= get_post_meta($post_id, '_sku', true);
					$range	= (array) get_post_meta($post_id, '_smartlink_price_range', true);
					$imp	= (array) get_post_meta($post_id, '_imprinting', true);

					if (empty($sku) || empty($range) || empty($imp)) {
						unset($post_id);
						$this->delete_product($post_id);
					}
				}
			}

			if (! isset($post_id) || $post_id <= 0) {
			    $post_id = wp_insert_post([
			    	'post_author'	=> $this->admin_id,
			        'post_title'	=> htmlspecialchars_decode($pname),
			        'post_content'	=> $product->Description,
			        'post_excerpt'	=> $product->ShortDescription,
			        'post_status'	=> 'publish',
			        'post_name'		=> sanitize_title($pname),
			        'post_type'		=> 'product'
			    ]);
			}

			$sku = $pnumber ?? (isset($product->Numbers) ? @$product->Numbers[0] : $product->Id);
			$stock_status = isset($product->HasInventory) && $product->HasInventory === false ? 'outofstock' : 'instock';
			$details = $shipping + [
				'stock_status' => $stock_status
			];

		    if (! $post_id)  return [
				'type'		=> "error",
				'message'	=> "Cannot add product to woocommerce for product: $item_id",
			];

		    @wp_set_object_terms($post_id, 'variable', 'product_type');
		    @wp_set_object_terms($post_id, $categories, 'product_cat');
		    @wp_set_object_terms($post_id, $themes, 'product_tag');
		    @wp_set_object_terms($post_id, $brands, 'yith_product_brand');

		    $this->insert_product_thumbnail($post_id, $product->ImageUrl);
		    if (isset($product->Images) && ! empty($product->Images)) $this->insert_product_images($post_id, $product->Images);
		    $this->insert_product_attributes($post_id, $attributes);
		    $this->insert_product_variations($post_id, $variations, $details);

		    update_post_meta($post_id, '_PID', $product->Id);
		    update_post_meta($post_id, '_PNU', $product->Number ?? @$product->Numbers[0]);
		    update_post_meta($post_id, '_is_variant', $is_variant ?? false);
		    update_post_meta($post_id, '_sku', $sku);
		    update_post_meta($post_id, '_weight', $shipping['weight']);
		    update_post_meta($post_id, '_length', $shipping['length']);
		    update_post_meta($post_id, '_width', $shipping['width']);
		    update_post_meta($post_id, '_height', $shipping['height']);
	        update_post_meta($post_id, '_virtual', 'no');
	        update_post_meta($post_id, '_downloadable', 'no');
	        update_post_meta($post_id, '_stock_status', $stock_status);
	        update_post_meta($post_id, '_sale_price', '');
		    update_post_meta($post_id, '_visibility', 'visible');
		    update_post_meta($post_id, '_regular_price', '');
		    update_post_meta($post_id, '_smartlink_price_range', $prices); // Used for price range table
		    update_post_meta($post_id, '_smartlink_info', $smartlink_info);
		    update_post_meta($post_id, '_imprinting', $imprinting);
		}

		return $post_id;
	}

	private function insert_product_attributes($product_id, $available_attributes)
	{
	    $product_attributes_data = [];

		foreach($available_attributes as $attribute => $attributes) {
			$term_ids	= [];
			$is_boolean	= false;
			$visible	= '1';

			if (in_array($attribute, [
				'_n',
				'quantity',
				'rush_service', // rush_service is used for variation while _rsv is for filters
			])) $visible = '0';

			if (is_bool($attributes)) {
				$is_boolean = true;
				$attribute = 'pa_' . sanitize_title($this->boolean_attrs[$attribute]);
				$attributes = [$attributes === true ? 'Yes' : 'No'];
			}

			foreach ($attributes as $term) {
		        $insert = wp_insert_term($term, $attribute, [
		            	'slug' => sanitize_title($term)
		            ]
		        );

				if (is_wp_error($insert)) {
					$term_id = $insert->error_data['term_exists'] ?? null;
				} else {
					$term_id = $insert['term_id'];
				}

				if ($term_id == null) continue;

				$term_ids[] = $term_id;
			}

			if ($is_boolean === true) {
			    wp_set_object_terms($product_id, $term_ids[0], $attribute);

			    $product_attributes_data[$attribute] = [
			        'name'			=> $attribute,
			        'value'			=> '',
			        'position'		=> 0,
			        'is_visible'	=> $visible,
			        'is_variation'	=> '0',
			        'is_taxonomy'	=> '1',
			    ];
			} else {
			    wp_set_object_terms($product_id, $term_ids, $attribute);

			    $product_attributes_data[$attribute] = [
			        'name'			=> ucfirst($attribute),
			        'value'			=> implode(' | ', $attributes),
			        'position'		=> 0,
			        'is_visible'	=> $visible,
			        'is_variation'	=> '1',
			        'is_taxonomy'	=> '0',
			    ];
			}

			if ($attribute == 'quantity') {
		    	update_post_meta($product_id, '_default_attributes', [$attribute => (string) min($attributes)]);
				update_post_meta($product_id, '_wpbo_deactive', '');
				update_post_meta($product_id, '_wpbo_override', 'on');
				update_post_meta($product_id, '_wpbo_step', '');
				update_post_meta($product_id, '_wpbo_minimum', min($attributes));
				update_post_meta($product_id, '_wpbo_maximum', '');
				update_post_meta($product_id, '_wpbo_minimum_oos', '');
				update_post_meta($product_id, '_wpbo_maximum_oos', '');
			}
		}

	    update_post_meta($product_id, '_product_attributes', $product_attributes_data);
	}

	private function insert_product_variations($product_id, $variations, $details)
	{
		$added_quantities = [];
		$total = count($variations);

	    foreach($variations as $index => $variation) {
	    	$post_id = wp_insert_post([
	    		'post_author'	=> $this->admin_id,
	            'post_title'	=> 'Variation #' . $index . ' of ' . $total . ' for product#' . $product_id,
	            'post_name'		=> 'product-' . $product_id . '-variation-' . $index,
	            'post_status'	=> 'publish',
	            'post_parent'	=> $product_id,
	            'post_type'		=> 'product_variation',
	            'guid'			=> home_url() . '/?product_variation=product-' . $product_id . '-variation-' . $index
	    	]);

			foreach ($variation['attributes'] as $attribute => $value) {

			    update_post_meta($post_id, 'attribute_' . $attribute, $value);
			}

	        update_post_meta($post_id, '_sku', '');
			update_post_meta($post_id, '_weight', $details['weight']);
			update_post_meta($post_id, '_length', $details['length']);
			update_post_meta($post_id, '_width', $details['width']);
			update_post_meta($post_id, '_height', $details['height']);
	        update_post_meta($post_id, '_default_attributes', []);
	        update_post_meta($post_id, '_virtual', 'no');
	        update_post_meta($post_id, '_downloadable', 'no');
	        update_post_meta($post_id, '_stock_status', $details['stock_status']);
	        update_post_meta($post_id, '_sale_price', '');
	        update_post_meta($post_id, '_price', $variation['price']);
	        update_post_meta($post_id, '_regular_price', $variation['price']);
	    }
	}

	private function attach_image($post_id, $image_url)
	{
		$image_url	= htmlspecialchars_decode($image_url, ENT_NOQUOTES);
		$upload_dir = wp_upload_dir();
		$image_data = @file_get_contents($image_url);
		$filename	= uniqid("$post_id-", true) . ".jpg";

		if (wp_mkdir_p($upload_dir['path'])) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		@file_put_contents($file, $image_data);

		$wp_filetype = wp_check_filetype($filename, null);

		$attachment = [
			'post_mime_type'	=> $wp_filetype['type'],
			'post_title'		=> sanitize_file_name($filename),
			'post_content'		=> '',
			'post_status'		=> 'inherit',
			'post_author'		=> $this->admin_id,
		];

		$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

		require_once(ABSPATH . 'wp-admin/includes/image.php');

		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

		wp_update_attachment_metadata( $attach_id, $attach_data );

		return $attach_id;
	}

	private function insert_product_thumbnail($post_id, $image_url)
	{
		$image_url = $this->base_url . '/v1/' . $image_url . '?size=normal';
		$attach_id = $this->attach_image($post_id, $image_url);

		set_post_thumbnail( $post_id, $attach_id );
	}

	private function insert_product_images($post_id, $image_urls)
	{
		$ids = explode(',', get_post_meta($post_id, '_thumbnail_id', true));

		// Unset the first image.
		unset($image_urls[0]);

		foreach ($image_urls as $image_url) {
			$image_url = $this->base_url . '/v1/' . $image_url . '?size=normal';
			$ids[] = $this->attach_image($post_id, $image_url);
		}

		$ids = array_unique($ids);

		update_post_meta( $post_id, '_product_image_gallery', implode(',', $ids) );
	}

	private function delete_product($post_id)
	{
		global $wpdb;

		wp_delete_post($post_id, true);
		wp_delete_attachment((int) get_post_meta($post_id, '_thumbnail_id', true), true );

        $meta = get_post_meta($post_id);

        foreach($meta as $key => $val) delete_post_meta($post_id, $key);

		$query = trim("
		SELECT * FROM $wpdb->posts
		WHERE post_author = '" . $this->admin_id . "'
		AND post_name LIKE '%product-$post_id-variation-%'
		AND post_type = 'product_variation'
		");

		$results = $wpdb->get_results($query);

		if ($results) {
			wp_delete_post($result->ID, true);
			
			foreach ($results as $result) {
				$meta = get_post_meta($result->ID);

				foreach($meta as $key => $val) delete_post_meta($result->ID, $key);				
			}
		}
	}

	private function send_smartlink_request(array $data = [])
	{
		if (
			empty($this->endpoint) ||
			empty($this->client_id) ||
			empty($this->client_secret)
		) return false;

		// Send HTTPS request to endpoint
		$response = wp_remote_request($this->endpoint, [
			'method'				=> 'GET',
			'timeout'				=> 60 * 60,
			'redirection'			=> 30,
			'httpversion'			=> '1.1',
			'reject_unsafe_urls'	=> true,
			'compress'				=> true,
			'decompress'			=> true,
			'headers'				=> "Authorization:AsiMemberAuth client_id=$this->client_id&client_secret=$this->client_secret",
		]);

		if (is_wp_error($response)) {
			// var_dump($response);
			$this->smartlink->log($response);

			return null;
		}

		return @json_decode($response['body']);
	}

	private function get_smartlink_search(array $query = [])
	{
		$query = http_build_query(wp_parse_args($query, [
			'q'		=> '', // The search query, must be URL encoded.
			'page'	=> '1', // Specifies the page to retrieve from.
			'rpp'	=> '100', // Number of product to return for each page.
			'sort'	=> 'PRNB', // Returns results ordered by the given sort value.
			'dl'	=> '', // Provides a list of relevant search dimensions to help narrow down the current result.
		]));

		$this->endpoint = $this->base_url . '/v1/products/search.json?' . $query;

		$response = $this->send_smartlink_request();

		if ($response === false ||
			! isset($response->Results) ||
			empty($response->Results)
		) {
			$this->smartlink->log($response);

			if (isset($response->Error) && $response->Error == 'Rate limit exceeded') exit;

			return null;
		}

		return $response;
	}

	private function get_smartlink_product($product_id)
	{
		$this->endpoint = $this->base_url . '/v1/products/' . $product_id . '.json';

		$response = $this->send_smartlink_request();

		if (
			$response === false ||
			! isset($response->Id) ||
			empty($response->Id)
		) {
			$this->smartlink->log($response);

			if (isset($response->Error) && $response->Error == 'Rate limit exceeded') exit;

			return null;
		}

		return $response;
	}
}

endif;

$smartlink = new SmartLink_ASI;
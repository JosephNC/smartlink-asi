<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die;

if ( ! class_exists( 'SmartLink_Public' ) ) : 

final class SmartLink_Public {

    private $slug = 'smartlink';
    private $smartlink;

    /**
     * The Constructor
     */
    public function __construct()
    {
        global $smartlink;
        
        if ( $smartlink->smartlink_inactive ) return;

        $this->smartlink = $smartlink;

        add_shortcode( SMARTLINK_SHORTCODE, [$this, 'smartlink_shortcode'] );
        add_filter( 'woocommerce_add_cart_item_data', [$this, 'add_cart_item_data'], 1, 4 );
        add_filter( 'woocommerce_get_cart_item_from_session', [$this, 'get_cart_items_from_session'], 1, 3 );
        add_filter( 'woocommerce_get_item_data', [$this, 'get_item_data'], 1, 2 );
        add_action( 'woocommerce_add_order_item_meta', [$this, 'add_order_item_meta'], 1, 2 );
        add_action( 'woocommerce_cart_calculate_fees', [$this, 'cart_calculate_fees'], 9999, 1 );
        add_action( 'woocommerce_before_calculate_totals', [$this, 'before_calculate_totals'], 9999, 1 );
        // add_filter( 'woocommerce_product_variation_title', [$this, 'product_variation_title'], 9999, 3 );
        // add_action( 'woocommerce_add_to_cart', [$this, 'add_to_cart'], 9999, 6 );

        add_action( 'woocommerce_after_cart_item_quantity_update', [$this, 'cart_item_quantity_update'], 9999, 4 );
        add_action( 'woocommerce_after_single_product_summary', [$this, 'price_range_table'], 1, 4 );
        add_action( 'wp_enqueue_scripts',  [&$this, 'scripts'], 10 );
    }

    public function product_variation_title($product, $title_base, $title_suffix)
    {
        var_dump($title_base);
        // var_dump($title_suffix);
        // var_dump($product);
        exit;
    }

    public function smartlink_shortcode($atts)
    {
        global $post;

        $a = shortcode_atts( [
            'type' => '',
        ], $atts );

        if (empty($a['type'])) return;

        $available = [
            'packaging',
            'production_time',
            'rush_time',
            'price_includes',
            'imprint_area',
            'additional_info',
        ];

        if (! in_array($a['type'], $available)) return;

        // Get the product meta
        $smartlink_info = get_post_meta($post->ID, '_smartlink_info', true);

        return $smartlink_info[$a['type']] ?? '&mdash;';
    }

    public function add_cart_item_data($cart_item_data, $product_id, $variation_id, $quantity, $post = [])
    {
        global $woocommerce;

        if (! get_product($product_id)->is_type( 'variable' )) return $cart_item_data;

        $post_imprint   = (empty($post)) ? (array) @$_POST['imprinting'] : (array) $post;
        $imprinting     = (array) get_post_meta($product_id, '_imprinting', true);
        $options        = [];
        $allowed        = [
            'color'     => 'imprinting_colors',
            'method'    => 'imprinting_methods',
            'service'   => 'imprinting_services',
            'location'  => 'imprinting_locations',
            'option'    => 'imprinting_options',
        ];

        foreach ($post_imprint as $type => $imprint) {
            if (! isset($allowed[$type])) continue;

            if ($type == 'option' && is_array($imprint)) {
                if ($imprint['accept'] != 'yes') continue;

                $phone_number = $imprint['phone'] ?? '';
                continue;
            }

            foreach ($imprinting[ $allowed[$type] ] as $value) {
                if ($value['name'] != $imprint) continue;

                $prices = [];

                foreach ($value['price'] as $type_code => $price) {
                    $prices[$type_code] = count($price) > 1 ? (float) $price[$quantity] : (float) reset($price);
                }

                if ($price != 0) {
                    $options[ $allowed[$type] ][] = [
                        '_name'     =>  $value['name'],
                        '_price'    =>  $prices,
                    ];
                }

                break;
            }
        }

        if (empty($options) && ! isset($phone_number)) return $cart_item_data;

        if (! empty($options))
            $cart_item_data['_imprinting'] = $options;

        if (isset($phone_number) && ! empty($phone_number))
            $cart_item_data['_imprinting_phone'] = $phone_number;

        return $cart_item_data;
    }

    public function get_cart_items_from_session($item, $values, $key)
    {
        if (array_key_exists( '_imprinting', $values ) ) {
            $item['_imprinting'] = $values['_imprinting'];
        }

        if (array_key_exists( '_imprinting_phone', $values ) ) {
            $item['_imprinting_phone'] = $values['_imprinting_phone'];
        }

        return $item;
    }

    public function get_item_data($item_data, $cart_item)
    {
        if (! isset($cart_item['_imprinting']) && ! isset($cart_item['_imprinting_phone'])) return $item_data;

        if (isset($cart_item['_imprinting'])) {
            foreach ($cart_item['_imprinting'] as $key => $imprinting) {
                $data[] = [
                    'key'   => ucwords(str_replace('_', ' ', $key)),
                    'value' => join("\n", array_map(function ($array) {
                        return str_replace('{{ delimiter }}', ': ', $array['_name']);
                    }, $imprinting)),
                    'hidden' => true,
                ];
            }
        }

        if (isset($cart_item['_imprinting_phone'])) {
            $data[] = [
                'key'   => 'Add. Phone Number',
                'value' => $cart_item['_imprinting_phone'],
                'hidden' => false,
            ];
        }

        $item_data = array_merge($item_data, $data);

        foreach ($item_data as $i => $item) {
            // $item_data[$i]['key'] = str_replace('_', ' ', $item['key']);
            if (strtolower($item_data[$i]['key']) == 'quantity') {
                $item_data[$i]['hidden'] = true;
                continue;
            } elseif (strtolower($item_data[$i]['key']) == 'rush_service') {
                $item_data[$i]['key'] = 'Rush Service';
            }
        }

        return $item_data;
    }

    public function add_order_item_meta($item_id, $values)
    {
        global $woocommerce, $wpdb;

        if (! isset($values['_imprinting']) && ! isset($values['_imprinting_phone'])) return;

        if (isset($values['_imprinting'])) {
            foreach ($values['_imprinting'] as $key => $imprinting) {

                $meta = join("\n", array_map(function ($array) {
                    return str_replace('{{ delimiter }}', ': ', $array['_name']);
                }, $imprinting));

                wc_add_order_item_meta($item_id, $key, $meta);
            }
        }

        if (isset($values['_imprinting_phone']))
            wc_add_order_item_meta($item_id, 'Add. Phone Number', $values['_imprinting_phone']);
    }

    public function add_to_cart($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data)
    {
        var_dump($cart_item_data);
        exit;
    }

    public function before_calculate_totals( $cart_object )
    {
        // $this->smartlink->log($cart_object);
        // return;

        foreach ( $cart_object->get_cart() as $cart_item_key => $value) {
            if (! isset($value['_imprinting'])) continue;

            $price = 0.00;

            // var_dump($value['_imprinting']);
            // exit;

            foreach ($value['_imprinting'] as $key => $imprinting) {
                $price += (float) array_sum(array_map(function ($data) {
                    $prices = 0.00;

                    foreach ($data['_price'] as $type_code => $_price) {
                        if ($type_code == 'STCH') continue;
                        
                        $prices += $_price;
                    }

                    return (float) $prices;
                }, $imprinting));
            }

            // $price = (float) array_sum($value['_imprinting']['_price']);
            $price = (float) $value['data']->get_regular_price() + $price;

            $value['data']->set_price((float) $price);
        }

        // var_dump($cart_object);
        // exit;
    }

    public function cart_calculate_fees( WC_Cart $cart )
    {
        // return;
        $fees = 0.00;

        foreach ( $cart->get_cart() as $cart_item_key => $value) {
            if (! isset($value['_imprinting'])) continue;

            foreach ($value['_imprinting'] as $key => $imprinting) {
                $fees += (float) array_sum(array_map(function ($data) {
                    return (float) @$data['_price']['STCH'] ?? 0.00;
                }, $imprinting));
            }
        }

        if ( $fees != 0 ) $cart->add_fee('Setup Charge', $fees);
    }

    public function cart_item_quantity_update($cart_item_key, $quantity, $old_quantity, $cart)
    {
        global $woocommerce, $product;

        $product = get_product($cart->cart_contents[$cart_item_key]['product_id']);

        if (! $product->is_type( 'variable' )) return;

        $available_variations = $product->get_available_variations();

        foreach($available_variations as $variation) {
            if (! isset($variation['attributes']['attribute_quantity'])) continue;
            if (! isset($variation['variation_is_active']) || $variation['variation_is_active'] !== true) continue;

            $attribute_quantity = $variation['attributes']['attribute_quantity'];

            if ($quantity >= $attribute_quantity) {
                $set = [
                    'id'    => $variation['variation_id'],
                    'qty'   => $attribute_quantity,
                ];
            }
        }

        if (! isset($set)) return;

        WC()->cart->cart_contents[$cart_item_key]['variation_id'] = $set['id'];
        WC()->cart->cart_contents[$cart_item_key]['variation']['attribute_quantity'] = $set['qty'];

        // $this->smartlink->log(WC()->cart->cart_contents[$cart_item_key]);

        // Update the imprinting
        $imprinting = $cart->cart_contents[$cart_item_key]['_imprinting'];
        $allowed        = [
            'imprinting_colors'     => 'color',
            'imprinting_methods'    => 'method',
            'imprinting_services'   => 'service',
            'imprinting_locations'  => 'location',
            'imprinting_options'    => 'option',
        ];

        foreach ($imprinting as $key => $imprint) {
            $data = array_map(function ($imp) {
                $split = explode('{{ delimiter }}', $imp['_name']);
                if (count($split) == 1) return trim($split[0]);

                return [
                    'name'  => trim($split[0]),
                    'value' => trim($split[1]),
                ];
            }, $imprint);

            $args[ $allowed[$key] ] = $data;
        }

        // $this->smartlink->log($args);

        $result = $this->add_cart_item_data(
            $cart->cart_contents[$cart_item_key],
            $cart->cart_contents[$cart_item_key]['product_id'],
            $set['id'],
            $quantity,
            $args
        );

        if (isset($result['_imprinting'])) {
            WC()->cart->cart_contents[$cart_item_key]['_imprinting'] = $result['_imprinting'];
        }

        // $this->smartlink->log(WC()->cart->cart_contents[$cart_item_key]);
    }

    public function price_range_table()
    {
        $post_id    = get_the_ID();
        $post_meta  = get_post_meta($post_id, '_smartlink_price_range', true);
        if (! $post_meta || empty($post_meta)) return;
        
        $quantities = array_keys($post_meta);
        $prices     = array_values($post_meta);
        ?>
        <table class="table rwd-table" style="margin: 2em 0;">
            <tbody>
                <tr style="background: #efefef;">
                    <th style="font-weight: normal;">Quantity:</th>
                    <?php foreach ($quantities as $quantity) :
                    if ($quantity == 0) continue;
                    ?>
                    <th style="font-weight: normal;"><?php echo $quantity; ?></th>
                    <?php endforeach; ?>
                </tr>

                <tr>
                    <td style="font-weight: normal;">Your Price <small>(each)</small>:</td>
                    <?php foreach ($prices as $price) :
                    if (is_object($price)) continue;
                    ?>
                    <th style="font-weight: normal;"><?php echo wc_price($price); ?></th>
                    <?php endforeach; ?>
                </tr>
            </tbody>
        </table>
        <?php
    }

    /**
     * Enqueue Public Scripts
     */
    public function scripts()
    {
        wp_enqueue_style( SMARTLINK_NAME, SMARTLINK_URL . 'assets/css/public.css', array(), SMARTLINK_VERSION, 'all' );

        wp_enqueue_script( SMARTLINK_NAME . '_lean_modal', SMARTLINK_URL . 'assets/js/lean-modal.min.js', array('jquery'), '1.1', true );        
        wp_enqueue_script( SMARTLINK_NAME, SMARTLINK_URL . 'assets/js/public.js', array('jquery'), SMARTLINK_VERSION, true );        
    }
}

endif;

if ( ! is_admin() || ( is_admin() && defined( 'DOING_AJAX' ) && DOING_AJAX ) ) new SmartLink_Public();
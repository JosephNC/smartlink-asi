<?php
/*
Plugin Name:    SmartLink ASI
Plugin URI:     https://josephnc.com/
Description:    Automatically imports your products from SmartLink ASI.
Version:        1.0
Author:         Chukwudiebube Joseph Nwakpaka
Author URI:     https://josephnc.com/
Text Domain:    smartlink
Domain Path:    /languages
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die;

if ( ! class_exists( 'SmartLink' ) ) :

final class SmartLink {
    
    /**
     * Check if SmartLink is running correctly.
     * @access public
     * @since 1.0
     */
    public $smartlink_inactive = false;
    
    /**
     * Is this SmartLink's first time on the site.
     * @access private
     * @since 1.0
     */
    private $is_new_install = false;
    
    /**
     * Check if we're upgrading SmartLink on the site.
     * @access private
     * @since 1.0
     */
    private $is_upgrading = false;
    
    /**
    * Holds all registered admin notices
    * 
    * @var mixed
    */
    public static $_notice = '';
    
    /**
     * Cloning is forbidden.
     * @since 1.0
     */
    public function __clone()
    {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'smartlink' ), '1.0' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     * @since 1.0
     */
    public function __wakeup()
    {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'smartlink' ), '1.0' );
    }
    
    /**
    * Construct
    * 
    */
    public function __construct()
    {
        $this->define_constants();

        $this->hooks();
        
        do_action( 'smartlink_loaded' );
    }
    
    /**
     * Define Constants.
     */
    private function define_constants()
    {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $plugin_data = get_plugin_data( __FILE__ );

        if ( ! defined( 'SMARTLINK_URL' ) ) define( 'SMARTLINK_URL', plugin_dir_url( __FILE__ ) );
        if ( ! defined( 'SMARTLINK_PATH' ) ) define( 'SMARTLINK_PATH', plugin_dir_path( __FILE__ ) );
        if ( ! defined( 'SMARTLINK_PLUGIN' ) ) define( 'SMARTLINK_PLUGIN', plugin_basename( __FILE__ ) );
        if ( ! defined( 'SMARTLINK_NAME' ) ) define( 'SMARTLINK_NAME', $plugin_data['Name'] );
        if ( ! defined( 'SMARTLINK_VERSION' ) ) define( 'SMARTLINK_VERSION', $plugin_data['Version'] );
        if ( ! defined( 'SMARTLINK_AUTHOR' ) ) define( 'SMARTLINK_AUTHOR', $plugin_data['Author'] );
        if ( ! defined( 'SMARTLINK_AUTHOR_URI' ) ) define( 'SMARTLINK_AUTHOR_URI', $plugin_data['AuthorURI'] );
        if ( ! defined( 'SMARTLINK_OPTIONS' ) ) define( 'SMARTLINK_OPTIONS', 'smartlink_options' );
        if ( ! defined( 'SMARTLINK_SHORTCODE' ) ) define( 'SMARTLINK_SHORTCODE', 'smartlink_asi' );
        if ( ! defined( 'SMARTLINK_REQUIRES_WC' ) ) define( 'SMARTLINK_REQUIRES_WC', '3.2' );
    }
    
    /**
    * Load Hooks
    */
    private function hooks()
    {
        add_action( 'plugins_loaded', array( $this, 'i18n' ), 1 );
        
        add_action( 'init', array( $this, 'plugin_check' ), 1 );
        
        add_action( 'init', array( $this, 'init' ), 2 );
        
        add_filter( 'plugin_action_links_' . SMARTLINK_PLUGIN, array( $this, 'plugin_links' ) );
    }
    
    /**
    * Internationalization
    *                        
    */
    public function i18n()
    {
        load_plugin_textdomain( 'smartlink', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }
    
    /**
    * Check Plugin Requirements
    * @since 1.0
    */
    public function plugin_check()
    {
        global $wpdb;

        if ( ! class_exists( 'woocommerce' ) ) {
             
            $this->add_notice(
                sprintf(
                    __( '<strong>%s</strong> requires WooCommerce to be activated to start working.', 'smartlink' ),
                    SMARTLINK_NAME
                ), 'error', false
            );
            
            $this->smartlink_inactive = true;
            
            return;
        
        }
        
        if ( version_compare( WC()->version, SMARTLINK_REQUIRES_WC, '<' ) ) {
             
            $this->add_notice(
                sprintf(
                    __( '<strong>%s</strong> requires a <a href="https://woocommerce.com/">newer version</a> of WooCommerce to work properly.', 'smartlink' ),
                    SMARTLINK_NAME
                ), 'error', false
            );
            
            $this->smartlink_inactive = true;

        }

        $options = get_option( SMARTLINK_OPTIONS, 'not exist' );
        $version = get_option( 'smartlink_version', 'not exist' );
        
        if ( $options == 'not exist' || $version == 'not exist' || version_compare( $version, SMARTLINK_VERSION, '<' ) ) {
            
            if ( $options == 'not exist' || $version == 'not exist' )
                $this->is_new_install = true;
            else
                $this->is_upgrading = true;
            
            if ( is_admin() && current_user_can( 'manage_options' ) ) $this->run_setup();
            
            $this->smartlink_inactive = true;   
            
        } elseif ( ! $options || ! $version || $version != SMARTLINK_VERSION ) {
            
            $notice = $this->add_notice(
                sprintf(
                    __( '<strong>%s</strong> needs some setup. <a href="%s">Run setup</a>', 'smartlink' ),
                    SMARTLINK_NAME,
                    add_query_arg( 'smartlink-setup', 'setup' )
                ), 'error', false
            );
            
            $this->smartlink_inactive = true;  
            
        }
    }
    
    /**
    * Initialize SmartLink
    */
    public function init()
    {
        // Before init action.
        do_action( 'before_smartlink_init' );
        
        // Do init check
        $this->init_check();

        // Load GLOBAL Variables
        $this->globals();        
        
        // Include/Require Files
        $this->includes();
        
        // Init action.
        do_action( 'smartlink_init' );
    }

    /**
    * Add Extra Links
    * 
    * @param array $links
    */
    public function plugin_links( $links )
    {
        $page = admin_url( 'admin.php?page=smartlink' );
        
        $settings_link = '<a href="' . $page . '">' . __( 'Settings', 'smartlink' ) . '</a>';
        $refresh_link = '
            <a id="smartlink-reset-btn" style="color:#b66517;" href="' .
                add_query_arg( 'smartlink-setup', 'setup' ) . '">' .
                __( 'Reset Plugin', 'smartlink' ) .
            '</a>';

        array_unshift( $links, $settings_link );
        array_push( $links, $refresh_link );
        
        return $links;
    }
    
    /**
    * Add Admin Notice
    * @since 1.0
    * @param string $msg The message to display
    * @param string $class The css class to add to notice "div" wrapper.
    * @param bool $dismiss Whether to add dismiss button.
    * @param bool $echo Whether to echo message instantly, Default is false. Message will be registered for display.
    */
    public function add_notice( $msg, $class, $dismiss = true, $echo = false )
    {
        if ( ! is_admin() ) return;
        
        if ( $dismiss === true ) {
            
            $dismiss = `<button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>`;
            
            $class .= ' is-dismissible';
            
        } else { $dismiss = ''; }
        
        $notice = '<div id="message" class="' . $class . ' notice"><p>' . $msg . '</p>' . $dismiss . '</div>';
        
        if ( $echo === true )
            echo $notice;
        else
            self::$_notice .= $notice;
    }

    /**
    * Run Setup
    * @since 1.0
    */
    public function run_setup()
    {
        // Prepare options
        $this->prepare_options();
        
        update_option( 'smartlink_version', SMARTLINK_VERSION );
        
        if ( $this->is_new_install ) {

            $page = 'admin.php?page=smartlink-start';
            
        } elseif ( $this->is_upgrading ) {

            $page = 'admin.php?page=smartlink-start&upgrade=1';
            
        } else {
            
            $page = 'admin.php?page=smartlink&smartlink-setup=complete';
            
        }

        // Remove some options
        delete_option( 'smartlink_category_ids' );
        delete_option( 'smartlink_product_exist' );
        delete_option( 'smartlink_product_page' );
        delete_option( 'smartlink_product_loop_offset' );
        delete_option( 'smartlink_product_loop_offset_partial' );
        delete_option( 'smartlink_product_end' );
        delete_transient( 'smartlink_boolean_attributes' );

        exit( wp_redirect( admin_url( $page ) ) );
    }

    /**
    * Do the Init method check
    * @since 1.0
    */
    private function init_check()
    {
        if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) return;

        if ( isset( $_REQUEST['smartlink-setup'] ) && $_REQUEST['smartlink-setup'] == 'setup' ) $this->run_setup();

        if ( isset( $_REQUEST['smartlink-setup'] ) && $_REQUEST['smartlink-setup'] == 'complete' ) {

            $this->add_notice(
                sprintf(
                    __( 'Thank you for using <strong>%s</strong>. Setup is complete.', 'smartlink' ),
                    SMARTLINK_NAME
                ), 'updated'
            );
        
        }
    }
    
    /**
    * SmartLink GLOBAL Variables
    * @since 1.0
    */
    private function globals()
    {
        $GLOBALS['smartlink_getting_started_tabs'] = array(
            'start' => __( 'Getting Started', 'smartlink' ),
        );
    }
    
    /**
    * Loads reqiured files
    * @since 1.0    
    */
    private function includes()
    {
        require_once SMARTLINK_PATH . 'core/admin.php';
        require_once SMARTLINK_PATH . 'core/smartlink.php';
        require_once SMARTLINK_PATH . 'core/public.php';
    }

    /**
    * Check if two arrays are equal
    * 
    * @param array $array1
    * @param array $array2
    */
    public function is_array_equal( $array1, $array2 )
    {
        if ( ! is_array( $array1 ) || ! is_array( $array2 ) ) return false;

        if ( array_diff_assoc( $array1, $array2 ) === array_diff_assoc( $array2, $array1 ) )
            return true;
        else
            return false;
    }

    /**
    * Prepare/Set the default SmartLink option
    * @since 1.0
    */
    private function prepare_options()
    {
        $options = get_option( SMARTLINK_OPTIONS, false );

        $data = array(
            'endpoint'      => 'https://api.stage-asicentral.com',
            'client_id'     => '',
            'client_secret' => '',
            'admin_id'      => get_current_user_id(),
        );

        if ( ! $options ) {

            update_option( SMARTLINK_OPTIONS, $data );

        } elseif ( count( $options ) < count( $data ) ) {

            $data = wp_parse_args( $options, $data );

            update_option( SMARTLINK_OPTIONS, $data );
        }

        update_option( 'smartlink-rewrite-flush', 0 );
    }

    /**
    * Returns the number of seconds/minutes/hour/days/weeks/months/year ago
    * 
    * @param string $time_1 The datetime to parse
    * @param string $time_2 The datetime to parse
    * @param boolean $complete Whether to return the full time ago. Default is false.
    * @param boolean $past Whether to return time ago or remaining time. Default is true.
    * 
    * @since 1.0
    */
    public static function time_difference( $time_1, $time_2, $complete = false, $past = true )
    {
        $time_1 = new DateTime( $time_1 ); // Initiate the DateInterval Class

        $time_2 = new DateTime( $time_2 ); // Initiate the DateInterval Class

        $diff = $time_1->diff( $time_2 );

        $diff->w = floor( $diff->d / 7 );

        $diff->d -= $diff->w * 7;

        $args = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );

        foreach ( $args as $key => &$value ) {
            
            if ( $diff->$key ) {
                
                $value = $diff->$key . ' ' . $value . ( $diff->$key > 1 ? 's' : '' );
                
            } else {
                
                unset( $args[$key] );
            }
        }

        if ( ! $complete ) $args = array_slice( $args, 0, 1 );

        if ( $past === true )
            $return = ( $args ) ? implode( ', ', $args ) . ' ago' : 'just now';
        else
            $return = ( $args ) ? implode( ', ', $args ) : 'a moment';

        return $return;
    }

    public function log($s)
    {
        $trace = debug_backtrace();

        // Dump the imediate trace
        $s = "\n";
        $s .= "\t" . "[file]" . "\t\t\t\t" . "=> " . $trace[0]['file'] . "\n";
        $s .= "\t" . "[line]" . "\t\t\t\t" . "=> " . $trace[0]['line'] . "\n";

        foreach ($trace[0]['args'] as $key => $arg) {
            if (is_array($arg)) {
                ob_start();
                print_r($arg);
                $arg = preg_replace("/[\n\r]/", "\n\t", ob_get_clean());
                preg_match("/(Array[\n\r\t]+\()([\s\S]+)(\))/i", $arg, $matches);
                $arg = $matches[2];
            } elseif (is_object($arg)) {
                $arg = json_encode($arg);
            }

            $s .= "\t" . "[args][$key]" . "\t\t\t" . "=> " . $arg . "\n";
        }

        error_log($s);
    }

    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int post id on success and 0 value otherwise
     */
    public function get_pid_by_meta($key, $value)
    {
        global $wpdb;

        $query = $wpdb->prepare("
        SELECT * FROM $wpdb->postmeta
        WHERE meta_key = %s
        AND meta_value = %s
        ", $key, $value);

        $meta = $wpdb->get_results($query);

        if (is_array($meta) && ! empty($meta) && isset($meta[0]))
            $meta = $meta[0];

        return is_object($meta) ? (int) $meta->post_id : 0;
    }
}

endif;

require_once __DIR__ . '/vendor/autoload.php';

$GLOBALS['smartlink'] = new SmartLink;
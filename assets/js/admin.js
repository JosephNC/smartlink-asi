jQuery(document).ready( function( $ ) {
    
    var url = new URI(),
        obj = {
            "action" : undefined,
            "_wpnonce" : undefined,
            "smartlink-setup" : undefined,
        };
        
    /**
    * Change window url
    * 
    * @param title
    * @param url
    */
    change_url = function( title, url ) {
        
        var obj = { Title: title, Url: url };
        
        history.pushState( obj, obj.Title, obj.Url );
        
    }
        
    url.removeQuery( obj );

    change_url( '', url );

});

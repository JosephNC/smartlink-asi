(function( $, window, document, undefined ) {

    /**
     * Decimal adjustment of a number.
     *
     * @param {String}  type  The type of adjustment.
     * @param {Number}  value The number.
     * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
     * @returns {Number} The adjusted value.
     */
    function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    /**
     * Format a number with grouped thousands
     * 
     * @param number The number being formatted
     * @param decimals Sets the number of decimal points
     * @param dec_point Sets the separator for the decimal point
     * @param thousands_sep Sets the thousands separator
     * @returns string A formatted version of number
     */
    function number_format( number, decimals, dec_point, thousands_sep ) {
        // Strip all characters but numerical ones.
        number = ( number + '' ).replace( /[^0-9+\-Ee.]/g, '' );

        var n = !isFinite( +number ) ? 0 : +number,
            prec = !isFinite( +decimals ) ? 0 : Math.abs( decimals ),
            sep = ( typeof thousands_sep === 'undefined' ) ? ',' : thousands_sep,
            dec = ( typeof dec_point === 'undefined' ) ? '.' : dec_point,
            s = '',
            toFixedFix = function ( n, prec ) {
                var k = Math.pow( 10, prec );
                return '' + ( Math.round( n * k ) / k ).toFixed(prec);
            };
            
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = ( prec ? toFixedFix( n, prec ) : '' + Math.round( n ) ).split( '.' );

        if ( s[0].length > 3 ) {
            s[0] = s[0].replace( /\B(?=(?:\d{3})+(?!\d))/g, sep );
        }

        if ( ( s[1] || '' ).length < prec ) {
            s[1] = s[1] || '';
            s[1] += new Array( prec - s[1].length + 1 ).join( '0' );
        }

        return s.join( dec );
    }

    var form = $('form.variations_form');
    var form_attr = form.attr('data-product_variations');
    var smartlink_item = form.find('.variations [for="quantity"]').parent().parent();
    var select = form.find('.variations select[name="attribute_quantity"]');
    var input_qty = form.find('.single_variation_wrap input[name="quantity"]');

    select.find('option[value=""]').remove();

    var options = select.find('option');

    var addImprinting = function() {
        if (typeof window.productPrice == 'undefined') return;

        var cost    = parseFloat(window.productPrice.amount),
            symbol  = window.productPrice.symbol,
            qty     = parseInt(input_qty.val()),
            html    = '',
            i_att   = $('.cntr_att select');

        for (var i = 0; i < i_att.length; i++) {
            var select  = i_att.eq(i).find('option:selected'),
                text    = select.val(),
                data    = select.attr('data-prices');

            if (typeof data == 'undefined') continue;

            data = JSON.parse(data);

            $.each(data, function (k, v) {
                if (k == 'STCH') return true; // Returm is it's setup charge.

                if (Object.keys(v).length > 1) {
                    cost += parseFloat(v[qty]);
                } else {
                    cost += parseFloat(v[Object.keys(v)[0]]);
                }
            });

            i_att.eq(i).parents('div.mb-item-variations').find('.mb-label .gray').text(text);
        };

        cost = number_format( decimalAdjust('round', cost, -2), 2, '.', ',' );

        html =
        `<span class="woocommerce-Price-currencySymbol">${symbol}</span>${cost}</span>`;

        $('.woocommerce-variation-price span.woocommerce-Price-amount.amount').html(html);
    }

    if (smartlink_item.siblings().length > 0) {
        smartlink_item.hide();
        form.find('.variations').show();
    }

    var quantities = [];

    $(document).on('click', 'a.reset_variations', function () {
        input_qty.trigger('change');
        $('.cntr_opt select option[value!=""]').eq(0).prop('selected', true);
        $('div.mb-item-variations').find('.mb-label .gray').text('');
    });

    input_qty.change(function () {
        var val = $(this).val(),
            current = parseInt(select.find(':selected').val()),
            set = 0;

        for (var i = 0; i < options.length; i++) {
            if (parseInt(val) >= parseInt(options.eq(i).val())) {
                set = i;
            }
        }

        if (parseInt(select.find('option')[set].value) != current) {
            select.find('option')[set].selected = true;

            select.trigger('change');
        }
    });

    $('.cntr_opt select').change(function(e) {
        e.preventDefault();

        var select  = $(this).find('option:selected'),
            text    = select.attr('data-text'),
            info    = $(this).parent('.cntr_opt').find('div.data-info');

        info.find('input.imp_info_phone').removeAttr('required');
        info.hide();

        if (typeof text == 'undefined' || text == '') return;

        info.find('input.imp_info_phone').attr('required', 'required');
        info.show().find('span.gray').text(text);
    });

    $( 'label.label-cbx' ).click( function () {
        var input = $(this).find('input.invisible'),
            first = $(this).siblings('select').find('option[value!=""]').eq(0);

        $(this).siblings('select').slideToggle('slow');
        input.prop('checked', (!input.is(':checked')) );

        // Reset the option
        first.prop('selected', true);
        select.trigger('change');

        addImprinting();

        return false;
    });

    $(document).ajaxStop(function () {
        var priceHtml   = $('.woocommerce-variation-price span.woocommerce-Price-amount.amount'),
            symbol      = priceHtml.find('.woocommerce-Price-currencySymbol').text(),
            price       = parseFloat(priceHtml.text().replace(symbol, ''));

        window.productPrice = {
            "amount": price,
            "symbol": symbol
        };

        addImprinting();
    });

    $('a.view-detail-button').leanModal({
        top : 200,
        overlay : 0.4,
        closeButton: '.view-modal-close'
    });

    $('a.view-detail-button').click(function(e) {
        e.preventDefault();

        var data = $(this).data('details'),
            i = 0,
            html = [];

        try {
            // data = JSON.parse(data);

            $.each(data, function(k, v) {
                if (k.toLowerCase() == 'total') {
                    var gen = v['cart_total'] + '&nbsp; + &nbsp;' + v['cost'] + ' &nbsp; = &nbsp; <strong><u>' + v['total'] + '</u></strong>';
                    html[i] = `<strong>${k}:</strong> <br>` +
                    'Cart Total: ' + v['cart_total'] + '<br>' +
                    'Imprinting Total: ' + v['cost'] + '<br><br>' +
                    '<strong>Item Cost:</strong> ' + gen;
                } else {
                    var text = [];

                    for (var j = 0; j < v.length; j++) {
                        text[j] = (j + 1) + '. ' + v[j];
                    };

                    html[i] = `<strong>${k}:</strong> <br>` + text.join('<br>');
                }

                i++;
            });

            html = html.join('<br><br>');

            $("#view-imprinting-details .content").html(html);
        } catch(e) {
            console.log(e);
            console.log(data);
        }

        return false;
    });

    $('.cntr_opt select').trigger('change');
    
}) ( jQuery, window, document );
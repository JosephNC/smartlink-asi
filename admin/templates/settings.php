<div class="wrap smartlink-wrapper">

    <h1><?php _e( 'SmartLink ASI Settings', 'smartlink' ); ?></h1>

    <form action="" method="POST">

        <?php wp_nonce_field( 'smartlink_nonce', 'smartlink_settings' ); ?>

        <h2><?php _e( 'SmartLink Settings', 'smartlink' ); ?></h2>
        <p><?php _e( 'This options affect how SmartLink connects to your SmartLink API.', 'smartlink' ); ?></p>

        <table class="form-table">
            <tbody>

                <tr>
                    <th scope="row"><label for="smartlink_endpoint"><?php _e( 'Endpoint', 'smartlink' ); ?></label></th>
                    <td><input type="url" class="regular-text url" id="smartlink_endpoint" name="smartlink_endpoint" value="<?php echo esc_attr( $endpoint ); ?>" required></td>
                </tr>

                <tr>
                    <th scope="row"><label for="smartlink_client_id"><?php _e( 'Client ID', 'smartlink' ); ?></label></th>
                    <td><input type="text" class="regular-text" id="smartlink_client_id" name="smartlink_client_id" value="<?php echo esc_attr( $client_id ); ?>" required></td>
                </tr>

                <tr>
                    <th scope="row"><label for="smartlink_client_secret"><?php _e( 'Client Secret', 'smartlink' ); ?></label></th>
                    <td><input type="text" class="regular-text" id="smartlink_client_secret" name="smartlink_client_secret" value="<?php echo esc_attr( $client_secret ); ?>" required></td>
                </tr>

            </tbody>
        </table>

        <p class="submit">
            <input type="submit" value="<?php _e( 'Save Changes', 'smartlink' ); ?>" class="button button-primary" name="smartlink_submit">
        </p>

    </form>

</div>
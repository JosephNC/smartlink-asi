<div class="changelog">
	<h3><?php _e( 'Getting Started', 'smartlink' ); ?></h3>
	<div class="feature-section">
		<p>
        <?php
        printf(
            __( '
            SmartLink ASI is a plugin for WooCommerce that helps you automatically import your products from SmartLink ASI server into
            <br>
            It also check for changes in the SmartLink ASI server and automatically updates it on WooCommerce. <br>
            %1$s has been designed to be easy to use. <br><br>
            About SmartLink: ASI SmartLink™ an application programming interface (API) that allows you to build a complete custom eCommerce solution.
            <br>
            You can see the full documentation at <a href="http://developers.asicentral.com/Smartlink/Index">ASI developer central</a>', 'smartlink'
            ),
            SMARTLINK_NAME
        ); ?>
        </p>
	</div>
</div>
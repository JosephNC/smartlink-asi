<div class="wrap smartlink-wrapper about-wrap smartlink-about-wrap">

    <?php if ( isset( $_GET['smartlink-upgrade'] ) && $_GET['smartlink-upgrade'] == 1 ) : ?>

	<h1><?php printf( __( 'Upgraded to %s Version %s', 'smartlink' ), SMARTLINK_NAME, SMARTLINK_VERSION ); ?></h1>
    
    <?php else : ?>
    
    <h1><?php printf( __( 'Welcome to %s', 'smartlink' ), SMARTLINK_NAME ); ?></h1>
    
    <?php endif; ?>

	<div class="about-text"><?php printf( __( '%s automatically imports your products from SmartLink ASI server into WooCommerce.', 'smartlink' ), SMARTLINK_NAME ); ?></div>

	<div class="wp-badge smartlink-badge"><?php printf( __( 'Version %s', 'smartlink' ), SMARTLINK_VERSION ); ?></div>
	
	<p class="smartlink-admin-notice smartlink-actions">
        <a href="<?php echo admin_url( 'admin.php?page=smartlink' ); ?>" class="button button-primary"><?php _e( 'Settings', 'smartlink' ); ?></a>
	</p>
    
    <h2 class="nav-tab-wrapper smartlink-nav-tab-wrapper">
    
    <?php
        
        foreach ( $smartlink_getting_started_tabs as $key => $label ) {
            
            $class = ( $key == $string ) ? 'nav-tab nav-tab-active' : 'nav-tab';
            
            $link = admin_url( 'admin.php?page=smartlink-' . $key );
            
            echo '<a href="' . $link . '" class="' . $class .'">' . $label . '</a>';
            
        }
    
    ?>
    
    </h2>
    <div class="changelog feature-list">
    
        <h2><?php _e( 'Need more help?', 'smartlink' ); ?></h2>
        
        <div class="feature-section">

            <p style="text-align:center;"><?php printf( __( 'If you have run into an issue with %s, you can contact us', 'smartlink' ), SMARTLINK_NAME ); ?></p>
            
            <p class="smartlink-admin-notice" style="text-align:center;">
                
                <a href="<?php echo SMARTLINK_AUTHOR_URI; ?>" target="_blank" class="button button-primary"><?php _e( 'Contact us', 'smartlink' ); ?></a>
            
            </p>
                                                                                                                         
        </div>
        
    </div>
    	
</div>
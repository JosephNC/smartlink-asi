=== SmartLink ASI ===
Contributors: ncej
Tags: ecommerce, e-commerce, store, sales, sell, shop, cart, checkout, smartlink, woocommerce
Requires at least: 4.4
Tested up to: 4.9.2
Stable tag: 1.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

SmartLink is a simple, WooCommerce plugin that helps you import products from smartlink server.

== Description ==

SmartLink is built to integrate seamlessly with WordPress and WooCommerce, it automatically imports your products from SmartLink ASI server periodically.
SmartLink also depends on other plugins to work efficiently:
1. [WooCommerce Advanced Product Quantities](https://wordpress.org/plugins/woocommerce-incremental-product-quantities/)
2. [Perfect WooCommerce Brands](https://wordpress.org/plugins/perfect-woocommerce-brands/)
3. [WooCommerce Advanced Product Quantities](http://www.wpbackoffice.com/plugins/woocommerce-incremental-product-quantities/)
4. [WP Control](https://wordpress.org/plugins/wp-crontrol/)

== Installation ==

= Minimum Requirements =

* PHP version 5.2.4 or greater (PHP 5.6 or greater is recommended)
* MySQL version 5.0 or greater (MySQL 5.6 or greater is recommended)
* WordPress 4.4+

= Manual installation =
Steps to get SmartLink working
1. Install and Activate dependencies.
2. Install and Activate SmartLink plugin.
3. Setup SmartLink by completing the SmartLink settings.
4. Create a cron job for SmartLink using the WP Control Plugin.
	*. Navigate to WP Control events.
	*. Under the Add Cron Events tab, add SmartLink cron job.
	*. Use `smartlink_cron_job` for Hook Name.
	*. Add the job interval and recurrence and save.
	*. Click on Run Now to see it at work.

== Changelog ==

= 1.0 =
* Initial Release